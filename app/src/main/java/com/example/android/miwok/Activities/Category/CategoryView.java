package com.example.android.miwok.Activities.Category;

import com.example.android.miwok.Navigators.RootNavigator;

public interface CategoryView {
    void configureWith(RootNavigator navigator);
}
