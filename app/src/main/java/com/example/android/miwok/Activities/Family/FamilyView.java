package com.example.android.miwok.Activities.Family;


public interface FamilyView {
    void configureWith(FamilyPresenter presenter);
}
