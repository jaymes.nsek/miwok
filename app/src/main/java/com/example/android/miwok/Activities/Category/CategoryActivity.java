/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.miwok.Activities.Category;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import com.example.android.miwok.Dependencies.DependencyRegistry;
import com.example.android.miwok.Navigators.RootNavigator;
import com.example.android.miwok.R;
import com.example.android.miwok.Activities.Colours.ColoursActivity;
import com.example.android.miwok.Activities.Family.FamilyActivity;
import com.example.android.miwok.Activities.Numbers.NumbersActivity;
import com.example.android.miwok.Activities.Phrases.PhrasesActivity;
import com.example.android.miwok.Helpers.ALCCallbacks;

public class CategoryActivity extends AppCompatActivity implements CategoryView {

    // Callback for retrieving activity in foreground.
    private final ALCCallbacks mCallbacks = new ALCCallbacks();
    private RootNavigator navigator;

    private TextView mNumbersCatTextView;
    private TextView mColoursCatTextView;
    private TextView mPhrasesCatTextView;
    private TextView mFamilyCatTextView;
    private TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Register Main with mCallbacks before calling Super.
        //getApplication().registerActivityLifecycleCallbacks(mCallbacks);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_activity);

        attachUI();

        DependencyRegistry.shared.inject(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Unregister callbacks after calling super.
        //getApplication().unregisterActivityLifecycleCallbacks(mCallbacks);
    }

    //region UI Interactions
    private void attachUI(){
        mNumbersCatTextView = findViewById(R.id.numbers);
        mColoursCatTextView = findViewById(R.id.colours);
        mPhrasesCatTextView = findViewById(R.id.phrases);
        mFamilyCatTextView = findViewById(R.id.family);

        attachListeners();
    }

    private void attachListeners(){
        setUpListeners(NumbersActivity.class, mNumbersCatTextView);
        setUpListeners(ColoursActivity.class, mColoursCatTextView);
        setUpListeners(PhrasesActivity.class, mPhrasesCatTextView);
        setUpListeners(FamilyActivity.class, mFamilyCatTextView);
    }

    /**
     * Sets onCLickListeners for all four Category Activities.
     */
    public void setUpListeners(Class<?> aClass, View view){
        textView = (TextView) view;

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageStartActivity(aClass);
            }
        });
    }
    //endregion

    //region Helper Methods
    private void manageStartActivity(Class<?> aClass){
        navigator.handleCategoriesClick(this, aClass);
    }
    //endregion

    //region CategoryView Interface
    @Override
    public void configureWith(RootNavigator navigator) {
        this.navigator = navigator;
    }
    //endregion
}
