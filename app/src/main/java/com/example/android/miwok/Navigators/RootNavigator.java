package com.example.android.miwok.Navigators;

import android.content.Context;
import android.content.Intent;

public class RootNavigator {

    //region CategoryActivity
    public void handleCategoriesClick(Context context, Class<?> aClass){
        Intent i = new Intent(context, aClass);
        context.startActivity(i);
    }
    //endregion
}
