package com.example.android.miwok.Model.DTOs

import androidx.annotation.DrawableRes
import androidx.annotation.RawRes

/**
 * Class for storing English/Miwok translation utilises Kotlin to elude boiler code.
 */
class Word @JvmOverloads constructor(val mDefaultTranslation: String, val mMiwokTranslation: String,
                                     @RawRes val mediaResourceId: Int = NONE_PROVIDED,
                                     @DrawableRes var imageResourceId: Int = NONE_PROVIDED){
    /**
     * Used by client class to determine whether instantiated object has an image association.
     */
    fun hasImage() = imageResourceId != NONE_PROVIDED

    override fun toString(): String = "Word(mDefaultTranslation='$mDefaultTranslation', " +
            "mMiwokTranslation=$mMiwokTranslation, mediaResourceId=$mediaResourceId, " +
                "imageResourceId=$imageResourceId)"
}

const val NONE_PROVIDED = -1