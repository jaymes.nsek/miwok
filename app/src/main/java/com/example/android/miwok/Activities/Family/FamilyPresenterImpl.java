package com.example.android.miwok.Activities.Family;

import android.content.Context;
import com.example.android.miwok.Helpers.ActivityHelpers.ListItemAudioManager;
import com.example.android.miwok.Model.DTOs.Word;
import com.example.android.miwok.Model.ModelLayer;

public class FamilyPresenterImpl implements FamilyPresenter {
    private final String TAG = getClass().getSimpleName();

    private ListItemAudioManager listItemAudioManager;
    private ModelLayer modelLayer;


    public FamilyPresenterImpl(ListItemAudioManager listItemAudioManager, ModelLayer modelLayer) {
        this.listItemAudioManager = listItemAudioManager;
        this.modelLayer = modelLayer;
    }

    @Override
    public void managePlayback(Context context, Word word) {
        listItemAudioManager.managePlayback(context, word);
    }

    @Override
    public boolean releaseMediaPlayer() {
        return listItemAudioManager.releaseMediaPlayer();
    }

}
