package com.example.android.miwok.Helpers.ActivityHelpers;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.util.Log;
import android.widget.AdapterView;
import androidx.annotation.RawRes;
import androidx.annotation.RequiresApi;
import com.example.android.miwok.Model.DTOs.Word;
import static android.content.Context.AUDIO_SERVICE;
import static android.media.AudioManager.AUDIOFOCUS_GAIN;
import static android.media.AudioManager.AUDIOFOCUS_GAIN_TRANSIENT;
import static android.media.AudioManager.AUDIOFOCUS_LOSS;
import static android.media.AudioManager.AUDIOFOCUS_LOSS_TRANSIENT;
import static android.media.AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK;
import static android.media.AudioManager.AUDIOFOCUS_REQUEST_GRANTED;
import static android.media.AudioManager.STREAM_MUSIC;


public class ListItemAudioManager {

    private final String TAG = getClass().getSimpleName();

    private static MediaPlayer mMediaPlayer;
    private static AdapterView.OnItemClickListener mItemClickListener;
    private AudioManager audioManager = null;
    private AudioAttributes playbackAttributes;
    private AudioFocusRequest focusRequest;
    private MediaPlayerManager mediaPlayerManager = null;
    private boolean isBuildVersionOreoPlus;


    /**
     * CONSTRUCTOR
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP_MR1|Build.VERSION_CODES.O)
    public ListItemAudioManager() {
        isBuildVersionOreoPlus = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O;
        mediaPlayerManager = new MediaPlayerManager();
    }

    @SuppressLint("NewApi")
    public void managePlayback(Context context, Word word){
        if (audioManager == null)
            audioManager = (AudioManager) context.getSystemService(AUDIO_SERVICE);

        int r = isBuildVersionOreoPlus ?
                requestAudioManagerOreo() : requestAudioManagerLollipop();

        if (r == AUDIOFOCUS_REQUEST_GRANTED)
            mediaPlayerManager.playMedia(context, word.getMediaResourceId());
        else
            Log.i(TAG, "onItemClick: ERROR");
    }

    private AudioManager.OnAudioFocusChangeListener audioFocusChangeListener =
            new AudioManager.OnAudioFocusChangeListener() {
                @Override
                public void onAudioFocusChange(int focusChange) {
                    handleAudioFocusChange(focusChange);
                }
            };

    private void handleAudioFocusChange(int focusChange){
        switch (focusChange){
            case AUDIOFOCUS_GAIN:
                mMediaPlayer.start();
                break;
            case AUDIOFOCUS_LOSS:
                releaseMediaPlayer();
                break;
            case AUDIOFOCUS_LOSS_TRANSIENT:
            case AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                // Pause playback and reset mediaPlayer to the start of file.
                mMediaPlayer.pause();
                mMediaPlayer.seekTo(0);
                break;
            default: throw new IllegalArgumentException();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private int requestAudioManagerOreo(){
        playbackAttributes = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_MEDIA)
                .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                .build();
        focusRequest = new AudioFocusRequest.Builder(AUDIOFOCUS_GAIN)
        .setAudioAttributes(playbackAttributes)
        .setAcceptsDelayedFocusGain(true)
        .setOnAudioFocusChangeListener(audioFocusChangeListener)
        .build();

        return audioManager.requestAudioFocus(focusRequest);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP_MR1)
    private int requestAudioManagerLollipop(){
        return audioManager.requestAudioFocus(audioFocusChangeListener, STREAM_MUSIC,
                AUDIOFOCUS_GAIN_TRANSIENT);
    }

    @SuppressLint("NewApi")
    public void releaseAudioFocus(){
        if (isBuildVersionOreoPlus) releaseAudioFocusOreo();
        else releaseAudioFocusLollipop();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void releaseAudioFocusOreo(){
        int state = audioManager.abandonAudioFocusRequest(focusRequest);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP_MR1)
    private void releaseAudioFocusLollipop(){
        int state = audioManager.abandonAudioFocus(audioFocusChangeListener);
    }

    /**
     * Delegated method.
     */
    public boolean releaseMediaPlayer(){
        Log.i(TAG, "releaseMediaPlayer: release called");
        return mediaPlayerManager.releaseMediaPlayer();
    }

    /**
     * Nested class contains the logic required to decipher and play media file from resID.
     */
    public class MediaPlayerManager {
        //region Singleton Listener fields
        /**
         * These listener fields ensure that for MediaPlayer processing new objects are not created
         *  instead these are reused to keep memory use low.
         */
        private MediaPlayer.OnPreparedListener mPreparedListener =
                new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        // play sound.
                        mp.start();
                    }
                };

        private MediaPlayer.OnCompletionListener mCompletionListener =
                new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        // release resources.
                        releaseMediaPlayer();
                        releaseAudioFocus();
                    }
                };
        //endregion


        private MediaPlayerManager() { }

        /**
         * Plays the media file associated with the received resource id.
         */
        public void playMedia(final Context context, @RawRes final int resId) {
            // Enforce release of resources just in-case another file is currently playing.
            releaseMediaPlayer();
            // Create the a MediaPlayer for a given resId.
            mMediaPlayer = MediaPlayer.create(context, resId);
            // set an onPreparedListener and when invoked start playing sound.
            mMediaPlayer.setOnPreparedListener(mPreparedListener);
            // set an onCompletionListener so that resources may be released once finished with.
            mMediaPlayer.setOnCompletionListener(mCompletionListener);
        }

        /**
         * Releases media player resources to free up memory.
         */
        public boolean releaseMediaPlayer(){
            if (mMediaPlayer != null){
                mMediaPlayer.release();
                mMediaPlayer = null;
                return true;
            }
            return false;
        }

    }

}
