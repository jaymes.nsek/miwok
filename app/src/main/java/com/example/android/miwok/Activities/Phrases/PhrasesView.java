package com.example.android.miwok.Activities.Phrases;

public interface PhrasesView {
    void configureWith(PhrasesPresenter presenter);
}
