package com.example.android.miwok.Model.Data;

import com.example.android.miwok.R;
import com.example.android.miwok.Model.DTOs.Word;

import java.util.ArrayList;

public class WordUtils {

    private WordUtils() {
        throw new AssertionError();
    }


    //region Populate Categories ArrayList

    /**
     * Helper method outlined within the region adds the content of words contained in its body to
     * the received ArrayList.
     * @param stringArrayList is the ArrayList to add words to.
     */
    public static void initialiseNumbersList(ArrayList<Word> stringArrayList){
        stringArrayList.add(new Word("one", "lutti",
                R.raw.number_one, R.drawable.number_one));
        stringArrayList.add(new Word("two", "otiiko",
                R.raw.number_two, R.drawable.number_two));
        stringArrayList.add(new Word("three", "tolookosu",
                R.raw.number_three, R.drawable.number_three));
        stringArrayList.add(new Word("four", "oyyisa",
                R.raw.number_four, R.drawable.number_four));
        stringArrayList.add(new Word("five", "massokka",
                R.raw.number_five, R.drawable.number_five));
        stringArrayList.add(new Word("six", "temmokka",
                R.raw.number_six, R.drawable.number_six));
        stringArrayList.add(new Word("seven", "kenekaku",
                R.raw.number_seven, R.drawable.number_seven));
        stringArrayList.add(new Word("eight", "kawinta",
                R.raw.number_eight, R.drawable.number_eight));
        stringArrayList.add(new Word("nine", "wo' e",
                R.raw.number_nine, R.drawable.number_nine));
        stringArrayList.add(new Word("ten", "na' aacha",
                R.raw.number_ten, R.drawable.number_ten));
    }

    public static void initialiseColoursList(ArrayList<Word> stringArrayList){
        stringArrayList.add(new Word("red", "weṭeṭṭi",
                R.raw.color_red, R.drawable.color_red));
        stringArrayList.add(new Word("green", "chokokki",
                R.raw.color_green, R.drawable.color_green));
        stringArrayList.add(new Word("brown", "ṭakaakki",
                R.raw.color_brown, R.drawable.color_brown));
        stringArrayList.add(new Word("gray", "ṭopoppi",
                R.raw.color_gray, R.drawable.color_gray));
        stringArrayList.add(new Word("black", "kululli",
                R.raw.color_black, R.drawable.color_black));
        stringArrayList.add(new Word("white", "kelelli",
                R.raw.color_white, R.drawable.color_white));
        stringArrayList.add(new Word("dusty yellow", "ṭopiisә",
                R.raw.color_dusty_yellow, R.drawable.color_dusty_yellow));
        stringArrayList.add(new Word("mustard yellow", "chiwiiṭә",
                R.raw.color_mustard_yellow, R.drawable.color_mustard_yellow));

    }

    public static void initialiseFamilyList(ArrayList<Word> stringArrayList){
        stringArrayList.add(new Word("father", "әpә",
                R.raw.family_father, R.drawable.family_father));
        stringArrayList.add(new Word("mother", "әṭa",
                R.raw.family_mother, R.drawable.family_mother));
        stringArrayList.add(new Word("son", "angsi",
                R.raw.family_son, R.drawable.family_son));
        stringArrayList.add(new Word("daughter", "tune",
                R.raw.family_daughter, R.drawable.family_daughter));
        stringArrayList.add(new Word("older brother", "taachi",
                R.raw.family_older_brother, R.drawable.family_older_brother));
        stringArrayList.add(new Word("younger brother",
                "chalitti", R.raw.family_younger_brother,
                R.drawable.family_younger_brother));
        stringArrayList.add(new Word("older sister", "teṭe",
                R.raw.family_older_sister, R.drawable.family_older_sister));
        stringArrayList.add(new Word("younger sister", "kolliti",
                R.raw.family_younger_sister, R.drawable.family_younger_sister));
        stringArrayList.add(new Word("grandfather", "paapa",
                R.raw.family_grandfather, R.drawable.family_grandfather));
        stringArrayList.add(new Word("grandmother", "ama" ,
                R.raw.family_grandmother, R.drawable.family_grandmother));
    }

    public static void initialisePhrasesList(ArrayList<Word> stringArrayList){
        stringArrayList.add(new Word("Where are you going?",
                "minto wuksus", R.raw.phrase_where_are_you_going));
        stringArrayList.add(new Word("What is your name?",
                "tinnә oyaase'nә", R.raw.phrase_what_is_your_name));
        stringArrayList.add(new Word("My name is...",
                "oyaaset...", R.raw.phrase_my_name_is));
        stringArrayList.add(new Word("How are you feeling?",
                "michәksәs?", R.raw.phrase_how_are_you_feeling));
        stringArrayList.add(new Word("I’m feeling good.",
                "kuchi achit", R.raw.phrase_im_feeling_good));
        stringArrayList.add(new Word("Are you coming?",
                "әәnәs'aa?", R.raw.phrase_are_you_coming));
        stringArrayList.add(new Word("Yes, I’m coming.",
                "hәә’ әәnәm", R.raw.phrase_yes_im_coming));
        stringArrayList.add(new Word("I’m coming.", "әәnәm",
                R.raw.phrase_im_coming));
        stringArrayList.add(new Word("Let’s go.", "yoowutis",
                R.raw.phrase_lets_go));
        stringArrayList.add(new Word("Come here.", "әnni'nem",
                R.raw.phrase_come_here));
    }

    //endregion
}
