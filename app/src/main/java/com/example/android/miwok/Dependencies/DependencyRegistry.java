package com.example.android.miwok.Dependencies;

import com.example.android.miwok.Activities.Category.CategoryView;
import com.example.android.miwok.Activities.Colours.ColoursPresenter;
import com.example.android.miwok.Activities.Colours.ColoursPresenterImpl;
import com.example.android.miwok.Activities.Colours.ColoursView;
import com.example.android.miwok.Activities.Family.FamilyPresenter;
import com.example.android.miwok.Activities.Family.FamilyPresenterImpl;
import com.example.android.miwok.Activities.Family.FamilyView;
import com.example.android.miwok.Activities.Numbers.NumbersPresenter;
import com.example.android.miwok.Activities.Numbers.NumbersPresenterImpl;
import com.example.android.miwok.Activities.Numbers.NumbersView;
import com.example.android.miwok.Activities.Phrases.PhrasesPresenter;
import com.example.android.miwok.Activities.Phrases.PhrasesPresenterImpl;
import com.example.android.miwok.Activities.Phrases.PhrasesView;
import com.example.android.miwok.Helpers.ActivityHelpers.ListItemAudioManager;
import com.example.android.miwok.Model.ModelLayer;
import com.example.android.miwok.Model.ModelLayerImpl;
import com.example.android.miwok.Navigators.RootNavigator;


public class DependencyRegistry {

    public static  DependencyRegistry shared = new DependencyRegistry();

    private ListItemAudioManager listItemAudioManager = new ListItemAudioManager();
    public RootNavigator navigator = new RootNavigator();

    public ModelLayer modelLayer = createModelLayer();
    private ModelLayer createModelLayer() {
        return new ModelLayerImpl();
    }


    public void inject(CategoryView view){
        view.configureWith(navigator);
    }

    public void inject(ColoursView view){
        ColoursPresenter presenter = new ColoursPresenterImpl(listItemAudioManager, modelLayer);
        view.configureWith(presenter);
    }

    public void inject(FamilyView view){
        FamilyPresenter presenter = new FamilyPresenterImpl(listItemAudioManager, modelLayer);
        view.configureWith(presenter);
    }

    public void inject(NumbersView view){
        NumbersPresenter presenter = new NumbersPresenterImpl(listItemAudioManager, modelLayer);
        view.configureWith(presenter);
    }

    public void inject(PhrasesView view){
        PhrasesPresenter presenter = new PhrasesPresenterImpl(listItemAudioManager, modelLayer);
        view.configureWith(presenter);
    }

}
