package com.example.android.miwok.Activities.Numbers;

import android.content.Context;
import com.example.android.miwok.Model.DTOs.Word;

public interface NumbersPresenter {
    void managePlayback(Context context, Word word);
    boolean releaseMediaPlayer();
}
