package com.example.android.miwok.Activities.Colours;

import android.content.Context;
import com.example.android.miwok.Model.DTOs.Word;

public interface ColoursPresenter {
    void managePlayback(Context context, Word word);
    boolean releaseMediaPlayer();
}
