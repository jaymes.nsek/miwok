package com.example.android.miwok.Activities.Family;

import android.content.Context;
import com.example.android.miwok.Model.DTOs.Word;

public interface FamilyPresenter {
    void managePlayback(Context context, Word word);
    boolean releaseMediaPlayer();
}
