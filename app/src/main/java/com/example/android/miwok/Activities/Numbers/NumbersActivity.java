package com.example.android.miwok.Activities.Numbers;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.ListView;
import com.example.android.miwok.Dependencies.DependencyRegistry;
import com.example.android.miwok.R;
import com.example.android.miwok.Model.DTOs.Word;
import com.example.android.miwok.Activities.WordAdapter;
import java.util.ArrayList;
import static com.example.android.miwok.Model.Data.WordUtils.initialiseNumbersList;

/**
 * SRP: This class serves to display number related words and their Miwok translations to User.
 */
public class NumbersActivity extends AppCompatActivity implements  NumbersView {
    
    private final String TAG = getClass().getSimpleName();

    private ListView listView;
    private NumbersPresenter presenter;
    private AdapterView.OnItemClickListener mItemClickListener =
            (adapterView, view, i, l) -> managePlayBack((Word) adapterView.getItemAtPosition(i));

    //region Lifecycle Methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.categories_list_view);

        attachUI();
        DependencyRegistry.shared.inject(this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (presenter.releaseMediaPlayer()){
            Log.i(TAG, getResources().getString(R.string.on_stop_log_msg));
        }
    }
    //endregion

    //region UI Interactions
    private void attachUI(){
        listView = findViewById(R.id.list_view);
        attachListener();
        initializeListView();
    }

    private void attachListener(){
        listView.setOnItemClickListener(mItemClickListener);
    }
    //endregion

    //region Helper Methods
    private void managePlayBack(Word word){
        presenter.managePlayback(this, word );
    }
    //endregion

    //region NumbersView Interface
    @Override
    public void configureWith(NumbersPresenter presenter) {
        this.presenter = presenter;
    }
    //endregion

    //region List View Adapter
    private void initializeListView() {
        ArrayList<Word> words = new ArrayList<>();
        initialiseNumbersList(words);
        WordAdapter adapter = new WordAdapter(this, words, R.color.category_numbers);
        listView.setAdapter(adapter);
    }
    //endregion
}
