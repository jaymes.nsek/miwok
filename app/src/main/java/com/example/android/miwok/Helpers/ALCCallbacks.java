package com.example.android.miwok.Helpers;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import androidx.annotation.NonNull;
import org.jetbrains.annotations.NotNull;


/**
 * Activity lifecycle callbacks.
 */
public class ALCCallbacks implements Application.ActivityLifecycleCallbacks {
    private Activity currentActivity;

    @Override
    public void onActivityCreated(@NonNull Activity activity, Bundle savedInstanceState) {
        currentActivity = activity;

        //Log.i(activity.getClass().getSimpleName(), "onCreate()");
    }

    @Override
    public void onActivityStarted(@NonNull Activity activity) {
        currentActivity = activity;

        //Log.i(activity.getClass().getSimpleName(), "onStart()");
    }

    @Override
    public void onActivityResumed(@NonNull Activity activity) {
        currentActivity = activity;

        //Log.i(activity.getClass().getSimpleName(), "onResume()");
    }

    @Override
    public void onActivityPaused(@NonNull Activity activity) {
        currentActivity = activity;

        //Log.i(activity.getClass().getSimpleName(), "onPause()");
    }

    @Override
    public void onActivityStopped(@NotNull Activity activity) {
        //Log.i(activity.getClass().getSimpleName(), "onStop()");
    }

    @Override
    public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle outState) {
        //Log.i(activity.getClass().getSimpleName(), "onSaveInstanceState()");
    }

    @Override
    public void onActivityDestroyed(@NonNull Activity activity) {
        //Log.i(activity.getClass().getSimpleName(), "onDestroy()");
    }

    public Activity getCurrentActivity() {
        return currentActivity;
    }

}
