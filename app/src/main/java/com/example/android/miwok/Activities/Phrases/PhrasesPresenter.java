package com.example.android.miwok.Activities.Phrases;

import android.content.Context;
import com.example.android.miwok.Model.DTOs.Word;

public interface PhrasesPresenter {
    void managePlayback(Context context, Word word);
    boolean releaseMediaPlayer();
}
