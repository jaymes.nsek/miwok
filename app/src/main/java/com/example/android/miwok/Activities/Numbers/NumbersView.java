package com.example.android.miwok.Activities.Numbers;

public interface NumbersView {
    void configureWith(NumbersPresenter presenter);
}
