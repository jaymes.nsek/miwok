package com.example.android.miwok.Activities.Family;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.ListView;
import com.example.android.miwok.Dependencies.DependencyRegistry;
import com.example.android.miwok.R;
import com.example.android.miwok.Model.DTOs.Word;
import com.example.android.miwok.Activities.WordAdapter;
import java.util.ArrayList;
import static com.example.android.miwok.Model.Data.WordUtils.initialiseFamilyList;

public class FamilyActivity extends AppCompatActivity implements FamilyView {

    private final String TAG = getClass().getSimpleName();

    private ListView listView;
    private FamilyPresenter presenter;
    private AdapterView.OnItemClickListener mItemClickListener =
            (adapterView, view, i, l) -> managePlayBack((Word) adapterView.getItemAtPosition(i));

    //region Lifecycle Methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.categories_list_view);

        attachUI();
        DependencyRegistry.shared.inject(this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (presenter.releaseMediaPlayer()){
            Log.i(TAG, getResources().getString(R.string.on_stop_log_msg));
        }
    }
    //endregion

    //region UI Interactions
    private void attachUI(){
        listView = findViewById(R.id.list_view);
        attachListener();
        initializeListView();
    }

    private void attachListener(){
        listView.setOnItemClickListener(mItemClickListener);
    }
    //endregion

    //region Helper Methods
    private void managePlayBack(Word word){
        presenter.managePlayback(this, word );
    }
    //endregion

    //region List View Adapter
    private void initializeListView() {
        ArrayList<Word> words = new ArrayList<>();
        initialiseFamilyList(words);
        WordAdapter adapter = new WordAdapter(this, words, R.color.category_family);
        listView.setAdapter(adapter);
    }
    //endregion

    //region FamilyView Interface
    @Override
    public void configureWith(FamilyPresenter presenter) {
        this.presenter = presenter;
    }
    //endregion

}
