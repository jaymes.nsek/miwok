package com.example.android.miwok.Activities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.example.android.miwok.Model.DTOs.Word;
import com.example.android.miwok.R;

import java.util.ArrayList;


/**
 * Adapter for populating ListView objects.
 */
public class WordAdapter extends ArrayAdapter<Word> {

    private int mColourResId;

    public WordAdapter(Context context, ArrayList<Word> words, @ColorRes int mColourResId){
        super(context, 0, words);

        this.mColourResId = mColourResId;
    }


    /**
     * @param parent is the LinearLayout of categories_list_view.xml.
     */
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        // Check if the existing View can be reused otherwise inflate one.
        View listItemView = convertView;

        if (listItemView == null)
            listItemView = LayoutInflater.from(getContext())
                    .inflate(R.layout.list_item, parent, false);

        // Get the Word object located at the passed position in the list.
        Word currentWord = getItem(position);

        assert currentWord != null : "Word object is NULL";

        // Find the Views corresponding to the various list_item.xml layout and set their text.
        TextView textView;

        textView = listItemView.findViewById(R.id.default_text_view);
        textView.setText(currentWord.getMDefaultTranslation());

        textView = listItemView.findViewById(R.id.miwok_text_view);
        textView.setText(currentWord.getMMiwokTranslation());

        ImageView imageView = listItemView.findViewById(R.id.list_image_view);
        if (currentWord.hasImage()){
            imageView.setVisibility(View.VISIBLE);
            imageView.setImageResource(currentWord.getImageResourceId());
        }
        else
            imageView.setVisibility(View.GONE);

        // Set list_item TextView's container mColourResId.
        listItemView.findViewById(R.id.text_containers)
                .setBackgroundColor(ContextCompat.getColor(getContext(), mColourResId));


        return listItemView;
    }

}
